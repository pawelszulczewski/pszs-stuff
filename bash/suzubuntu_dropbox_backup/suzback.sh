#!/bin/bash

DATE=`date +%F`
FILENAME=/tmp/suzubuntu_$DATE.tar
DBFILENAME=/usr/src/wiki/suzubuntu_db_$DATE.sql
MAINBACKUPDIR=/home/psz/backup/
TMPMAILFILE=/tmp/.tmpmail

# Lista adresowa (na którą będą wysyłane maile).
# Adresy oddzielać od siebie średnikami (chyba).
RCPTLIST=foo@bar.pl

# Sprawdzenie czy demon dropbox-a jest uruchomiony.
# Jeśli nie -- podejmowana jest próba odpalenia.
# Jeśli z kolei próba odpalenia się nie powiedzie,
# wówczas wysyłany jest mail.
# DROPBOXPRCS=`ps auxc | grep -v grep | grep dropbox -c`
# if [ $DROPBOXPRCS -eq 0 ]
#then
#     dropbox start && DROPBOXPRCS=`ps auxc | grep -v grep | grep dropbox -c`
#    if [ $DROPBOXPRCS -eq 0 ]
#    then
#	echo "Ciao!" > $TMPMAILFILE	
#	echo "------------------------" >> $TMPMAILFILE
#	echo "Lokalizacja: Suzubuntu" >> $TMPMAILFILE
#	echo "Backup" >> $TMPMAILFILE
#	echo "BŁĄD: NIE MOŻNA URUCHOMIĆ USŁUGI DROPBOX-A" >> $TMPMAILFILE
#	echo "Backup NIE BĘDZIE przechowywany na koncie DropBoxa." >> $TMPMAILFILE
#	mutt -s "[SUZUBUNTU] [BACKUP] Nie uruchomiona usługa DropBox-a" -i $TMPMAILFILE $RCPTLIST < /dev/null
#	rm $TMPMAILFILE
#    else
#	echo "Ciao!" > $TMPMAILFILE
#	echo "------------------------" >> $TMPMAILFILE
#	echo "Lokalizacja: Suzubuntu" >> $TMPMAILFILE
#	echo "Backup" >> $TMPMAILFILE
#	echo "Zdarzenie: Uruchomiona została usługa DropBox-a" >> $TMPMAILFILE
#	mutt -s "[SUZUBUNTU] [BACKUP] Usługa DropBox-a została uruchomiona" -i $TMPMAILFILE $RCPTLIST < /dev/null
#	rm $TMPMAILFILE
#   fi
#fi

# if [ $DROPBOXPRCS -eq 0 ]
# then
#    dropbox start
#else
#    killall dropbox && dropbox start
# fi

# Najprościej
killall dropbox && dropbox start

# Tworzenie, pakowanie, szyfrowanie i przenoszenie backupu
# do glownego katalogu backupu
mysqldump --all-databases --opt -u root -pscretdatabasepass > $DBFILENAME
tar -cf $FILENAME /usr/src/wiki/* /home/subversion/* /etc/*
gpg --quiet --encrypt -r suzubuntu@suzuki $FILENAME && cp $FILENAME.gpg $MAINBACKUPDIR

# 2 GiB DropBox-a w bajtach
MAGICDPBNUMBER=2147483648
DROPBOXDIR=/home/psz/Dropbox

# najwczesniejszy z zaladowanych backupow i jego wielkosc
LASTBKFILE=`ls -l1t $DROPBOXDIR/suzubuntu_* | tail -1`
NAMELASTBK=`echo $LASTBKFILE | cut -d" " -f8`
SIZELASTBK=`echo $LASTBKFILE | cut -d" " -f5`

# Czy nowy backup zmiesci sie na Dropbox-ie?
SIZENEWBK=`ls -l $FILENAME.gpg | cut -d" " -f5`
# Oho, mistrzostwo swiata. Wie ktos jak W PROSTSZY sposób sprawdzic ile zajmuje jednen katalog?
DROPBOXSZ=`du -bs $DROPBOXDIR | perl -pe 'if ($_ =~ m/[0-9]+/) {print "$&\n"};' | head -1`
# Ile zostanie miejsca na DropBox-ie po usunieciu najwczesniejszego backupu i nagraniu najnowszego?
FREESPACE=`expr $MAGICDPBNUMBER - $DROPBOXSZ - $SIZELASTBK + $SIZENEWBK`
FREESPACEINKBS=`echo "scale=2;$FREESPACE/1024" | bc`

if [ $FREESPACE -le 0 ] 
then  
    #Akcja Brak-Miejsca-Do-Załadowania-Nowego-Backupu
    echo "Ciao!" > $TMPMAILFILE
    echo "--------------------------------------------------------" >> $TMPMAILFILE
    echo "Lokalizacja: Suzubuntu" >> $TMPMAILFILE
    echo "Backup" >> $TMPMAILFILE
    echo "BŁĄD: BRAK MIEJSCA NA KONCIE DropBox-a" >> $TMPMAILFILE
    echo "--------------------------------------------------------" >> $TMPMAILFILE
    echo "MagicDPBNumber: $MAGICDPBNUMBER" >> $TMPMAILFILE
    echo "DropBoxSize: $DROPBOXSZ" >> $TMPMAILFILE
    echo "SizeLastBk: $SIZELASTBK" >> $SIZELASTBK
    echo "SizeNewBk: $SIZENEWBK" >> $SIZENEWBK
    echo "Freespace: $FREESPACE" >> $FREESPACE
    mutt -s "[SUZUBUNTU] [BACKUP] Brak miejsca na koncie DropBoxa" -i $TMPMAILFILE $RCPTLIST < /dev/null
    rm $TMPMAILFILE
else
    #Akcja Ładujemy-Nowy-Backup 
    rm $NAMELASTBK
    mv $FILENAME.gpg $DROPBOXDIR
    echo "Ciao!" > $TMPMAILFILE
    echo "---------------------------------------------" >> $TMPMAILFILE
    echo "SUZUBUNTU, raport z wykonania kopii zapasowej" >> $TMPMAILFILE
    echo "---------------------------------------------" >> $TMPMAILFILE
    echo "Filename: $FILENAME.gpg" >> $TMPMAILFILE
    echo "DropBoxFreeSpace: $FREESPACEINKBS KiB" >> $TMPMAILFILE
    mutt -s "[SUZUBUNTU] [BACKUP] Raport wykonania kopii zapasowej" -i $TMPMAILFILE $RCPTLIST < /dev/null
    rm $TMPMAILFILE    
fi

# Sprzatanie
rm $FILENAME
rm $DBFILENAME