/* Pac-Man made with ncurses by SirTopHat.
 Credit also to Sinuvoid for fixing an array/pointer issue.
 See http://forbidden.legend-zone.com/                  
 cleanup, organization, and restructuring by spinout
 
 to compile, type the following at the command line:
  sudo apt-get install libncurses-dev build-essential
  gcc -o paccurse pacmancurse.c -lncurses
*/

#include <curses.h>
/* "incompatible implicit declaration" warnings are brought up if these
 headers are not included */
#include <stdlib.h>	/* malloc */
#include <string.h>	/* memcpy */

typedef struct {
 int x, y;	/* position */
 char c;	/* charachter */
 char dir;	/* direction (ghosts only) */
} coordchar;

#define KEY_ESC	27
#define SCORES_FN	".pacscores"

/*
 pick up a collision between two coordchar objects
*/
#define collision(a, b)\
	(a.x == b.x && a.y == b.y)

/*
 calculate position within array from x y position
*/
#define arraypos(y, x)\
	(61 * (y) + (x))

/*
 draw a coordchar object
*/
#define draw_coordchar(xyc)\
{\
    move(xyc.y, xyc.x);\
    addch(xyc.c);\
}

static char background[] = 
{
"| |||||||||||||||||||||||||||||||||||||||||||||||||||||||| |\n"
"|                                                          |\n" 
"| ||||||||||||||||||||||||||| |||||||||||||||||||||||||||| |\n" 
"|                                                          |\n" 
"| |||||||||||||||||||||||||||||||||||||||||||||||||||||||| |\n" 
"|                                                          |\n"
"| |||||||||||||||| ||||||||||--|||||||||| |||||||||||||||| |\n"
"                   |--------------------|                   \n"
"| |||||||||||||||| |||||||||||||||||||||| |||||||||||||||| |\n"
"|                                                          |\n"
"| |||||||||||||||||||||||||||||||||||||||||||||||||||||||| |\n"
"|                                                          |\n"
"| |||||||||||||||||||||||||||| ||||||||||||||||||||||||||| |\n"
"|                                                          |\n" 
"| |||||||||||||||||||||||||||||||||||||||||||||||||||||||| |\n"
};


static char foreground[] =
{
"| |||||||||||||||||||||||||||||||||||||||||||||||||||||||| |\n"
"|**********************************************************|\n"
"|*|||||||||||||||||||||||||||*||||||||||||||||||||||||||||*|\n"
"|**********************************************************|\n"
"|*||||||||||||||||||||||||||||||||||||||||||||||||||||||||*|\n"
"|**********************************************************|\n"
"|*||||||||||||||||*||||||||||--||||||||||*||||||||||||||||*|\n"
" ******************|--------------------|****************** \n"
"|*||||||||||||||||*||||||||||||||||||||||*||||||||||||||||*|\n"
"|**********************************************************|\n"
"|*||||||||||||||||||||||||||||||||||||||||||||||||||||||||*|\n"
"|**********************************************************|\n"
"|*||||||||||||||||||||||||||||*|||||||||||||||||||||||||||*|\n"
"|**********************************************************|\n"
"| |||||||||||||||||||||||||||||||||||||||||||||||||||||||| |\n"
};

static char foreground1[] = 
{
"| |||||||||||||||||||||||||||||||||||||||||||||||||||||||| |\n"   
"|**********************************************************|\n" 
"|*|||||||||||||||||||||||||||*||||||||||||||||||||||||||||*|\n"   
"|**********************************************************|\n" 
"|*||||||||||||||||||||||||||||||||||||||||||||||||||||||||*|\n"  
"|**********************************************************|\n" 
"|*||||||||||||||||*||||||||||--||||||||||*||||||||||||||||*|\n" 
" ******************|--------------------|****************** \n"
"|*||||||||||||||||*||||||||||||||||||||||*||||||||||||||||*|\n"
"|**********************************************************|\n" 
"|*||||||||||||||||||||||||||||||||||||||||||||||||||||||||*|\n"  
"|**********************************************************|\n"  
"|*||||||||||||||||||||||||||||*|||||||||||||||||||||||||||*|\n"   
"|**********************************************************|\n"  
"| |||||||||||||||||||||||||||||||||||||||||||||||||||||||| |\n"
};

int slowdown=0;
/*
 function declarations
*/
coordchar boundries(coordchar xy);
coordchar ghost_update(coordchar xy);
int randint(int min, int max);
void display_scores();
void save_score(int level, int score, char * name);

/*
 save score
*/
void save_score(int level, int score, char * name)
{
    FILE* scores = fopen(SCORES_FN,"a+b");
    fseek(scores, feof(scores), SEEK_SET);
    fprintf(scores,"%s level %i, %i points.\n",name,level,score);
    fclose(scores);
}

/*
 Show scores
*/

void display_scores()
{
    char * bytes;
    size_t sz;
    FILE* scores = fopen(SCORES_FN,"r");
    if(scores != NULL){
        fseek(scores, 0L, SEEK_END);
        bytes = malloc(sz = ftell(scores));
        rewind(scores);
        fread(bytes, sz, 1, scores);
        fclose(scores);
        
        printf("Previous scores:\n%s\n", bytes);
    }
    else
        printf("No previous scores\n");
}
    
    
/*
 update ghost position
*/
coordchar ghost_update(coordchar xy)
{
    coordchar backup = {xy.x, xy.y, xy.c, xy.dir};
    
    (!xy.dir) ? xy.x++ : (xy.dir==1) ? xy.x-- : (xy.dir==2) ? xy.y++ : xy.y--;
    
    /* collision with wall */
    while ( background[ arraypos(xy.y, xy.x) ] != ' '){
        
        memcpy(&xy, &backup, sizeof(coordchar));
        dirchange:
        xy.dir = randint(0,4);
        (!xy.dir) ? xy.x++ : (xy.dir==1) ? xy.x-- : (slowdown) ? 0 : (xy.dir==2) ? xy.y++ : xy.y--;
    }
    /* randomly change direction */
    if(!randint(0,7))
        goto dirchange;
    
    xy = boundries(xy);
    draw_coordchar(xy);
    return xy;
}

/*
 force a coordchar object to be within boundries; bring it to the
 other side if it is outside of the boundries
*/
coordchar boundries(coordchar xy)
{
    xy.x = (xy.x > 59) ? 0 : (xy.x < 0) ? 59 : xy.x;
    xy.y = (xy.y > 14) ? 0 : (xy.y < 0) ? 14 : xy.y;
    return xy;
}


/*
 random integer
*/
int randint(int min, int max)
{
    int x;
    for(;;)
    {
        x = random()%max;
        if (x>=min & x<=max)
            break;
    }
    return x; 
}

int main(int argc, char *argv[])
{
    if (argc > 1)
    {
        if(!strcmp(argv[1], "-scores")){
            display_scores();
            exit(EXIT_SUCCESS);
        }
        
        /* initialize variables */
        int i;
        int keypress, lastkey=0, lastlastkey=0, pellets=0, lives=3, slowdown=0, counter=3;
        int level=1, score=0;
        
        coordchar playerold;
        coordchar ghosts_old[4];

       
        /* set up */
        initscr();
        refresh();
 
        noecho();
        raw();
        cbreak();

        timeout(0);

        keypad(stdscr, TRUE);
        
        for(;;
          /* stuff to do each session */
            refresh(),
            level++,		/* increase level */
            pellets=0,		/* set pellet count to 0 */
            lives=3,		/* set lives to 3 */
            lastkey = 0,	/* set keypress */
            lastlastkey = 0,
            /* set background */
            memcpy(&foreground, &foreground1, sizeof(foreground1)),
            counter = 3		/* timer */
        )
        {
            coordchar player = {30, 11, 'o', 0};
            coordchar ghosts[4] = {
             {1, 3,   'G', randint(0,4)},
             {59, 3,  'G', randint(0,4)},
             {59, 13, 'G', randint(0,4)},
             {1, 13,  'G', randint(0,4)}
            };
            
            /* countdown */
            while (counter)
            {
                clear();
                move(0,0);
                printw("%s",background);
                move(11,20);
                printw("Starting in %i",counter);
                counter--;
                refresh();
                sleep(1);
            }
            
            /* main gameplay loop */
            for(;;
            /* stuff to do each loop */
                 clear(),			/* clear screen */
                 move(0,0),
                 printw("%s", foreground),	/* draw foreground */
                 memcpy(&playerold, &player, sizeof(coordchar)), /* backup position */
                 slowdown = !slowdown,		/* toggle slowdown */
                 keypress = getch()		/* current key */
            )
            {                
                for(i=0;i<sizeof(ghosts)/sizeof(coordchar);i++)
                   memcpy(&ghosts_old[i], &ghosts[i], sizeof(coordchar));
                
                if (keypress >  0)
                    lastkey = keypress;
                
                calcdir:
                switch(lastkey)
                {
                case KEY_RIGHT:
                    player.c = '<';
                    player.x++;
                    break;
                case KEY_LEFT:
                    player.c = '>';
                    player.x--;
                    break;
                case KEY_DOWN:
                    player.c = '^';
                    if(slowdown)
                        player.y++;
                    break;
                case KEY_UP:
                    player.c = 'v';
                    if(slowdown)
                        player.y--;
                    break;
                case KEY_ESC:
                    refresh();
                    endwin();
                    exit(EXIT_SUCCESS);
                }
                
                //keeping the player within the borders
                player = boundries(player);
                
                //Collision part 2
                //Pacman
                if ( background[ arraypos(player.y,player.x) ] != ' ' ){
                    memcpy(&player, &playerold, sizeof(coordchar));
                    if(lastkey != lastlastkey){
                        lastkey = lastlastkey;
                        goto calcdir;
                    }
                    lastkey = lastlastkey;
                }
                else
                    lastlastkey = lastkey;
                
                //pacman frames
                player.c = (slowdown) ? 'o' : player.c;
                
                //Move along, Pacman, nothing to see here.
                draw_coordchar(player);
                
                //update ghosts
                for(i=0;i<sizeof(ghosts)/sizeof(coordchar);i++)
                    ghosts[i] = ghost_update(ghosts[i]);
                
                //Pellets
                if (foreground[arraypos(player.y,player.x)] != background[arraypos(player.y,player.x)]) 
                {
                    foreground[arraypos(player.y,player.x)] = background[arraypos(player.y,player.x)];
                    score++;
                    pellets++;
                }
                if ( pellets == 402 )
                    break;
                
                //Score
                move( 0, 61 );
                printw("Score: %i",score);
                move( 1, 61 );
                printw("Level: %i",level);
                move( 2, 61 );
                printw("Lives: %i",lives-1);
                
                //Ghost collision with player
                for(i=0;i<sizeof(ghosts)/sizeof(coordchar);i++){
                    if(collision(player, ghosts[i])){
                        lives--;
                        player.x = 30;
                        player.y = 11;
                        player.c = 'o';
                        lastkey = 0;
                        break;
                    }
                }
                
                //Game over. man, game over
                if (!lives)
                {
                    save_score(level, score, argv[1]);
                    level = 0;
                    score = 0;
                    move(11,10);
                    printw( "Game over, saving score to score list. " );
                    refresh();
                    sleep(2);
                    break;
                }
                refresh();
                usleep(100000-(level*100));
            }
        }
        refresh();
        endwin();

    }

    else
    {
        printf("Usage: pacman [your name] or pacman -scores\n");
        exit(EXIT_SUCCESS);
    }
}
